scriptencoding utf-8

" ==============================================================================
" === Neovim configuration file - init.vim
" ==============================================================================

" ===== Environment Variables =====
if $XDG_CACHE_HOME ==# ''
  let $XDG_CACHE_HOME = $HOME.'/.cache'
endif

if $XDG_CONFIG_HOME ==# ''
  let $XDG_CONFIG_HOME = $HOME.'/.config'
endif

" Set Leader Keys
let g:mapleader = ' '
let g:maplocalleader = ','

" ==============================================================================
" === Plugins
" ==============================================================================

" Autoinstall vim-plug
if empty(glob('~/.config/nvim/autoload/plug.vim'))
  silent !curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  augroup plugged
    autocmd! VimEnter * PlugInstall --sync | source $MYVIMRC
  augroup END
endif

call plug#begin('~/.config/nvim/plugged')

" ===== Display/Interface =====
" Colorschemes
Plug 'bluz71/vim-moonfly-colors'
Plug 'ellisonleao/gruvbox.nvim'

Plug 'majutsushi/tagbar'

Plug 'mbbill/undotree'

" ===== Files =====
Plug 'junegunn/fzf', { 'dir' : '~/.fzf', 'do' : './install --all' }

Plug 'junegunn/fzf.vim'
  let g:fzf_layout = { 'window' : 'enew' }

Plug 'scrooloose/nerdtree'
  let g:NERDTreeAutoDeleteBuffer = 1
  let g:NERDTreeMinimalUI = 1

Plug 'Xuyuanp/nerdtree-git-plugin'

" ===== Autocomplete/Snippets =====
Plug 'SirVer/ultisnips'
  let g:UltiSnipsSnippetsDir = '~/.config/nvim/UltiSnips'
  let g:UltiSnipsEditSplit = 'context'
  let g:UltiSnipsExpandTrigger = '<C-b>'
  let g:UltiSnipsJumpForwardTrigger = '<C-j>'
  let g:UltiSnipsJumpBackwardTrigger = '<C-k>'

Plug 'honza/vim-snippets'

" ===== Git =====
Plug 'tpope/vim-fugitive'

"Plug 'airblade/vim-gitgutter'
Plug 'lewis6991/gitsigns.nvim'

" ===== Languages/Syntax =====
Plug 'w0rp/ale'
  let g:airline#extensions#ale#enabled = 0

Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-neorg/neorg', {'do': ':Neorg sync-parsers'}

" LaTeX
Plug 'lervag/vimtex'

" ===== Miscellaneous =====
Plug 'ludovicchabant/vim-gutentags'
  let g:gutentags_cache_dir = $XDG_CACHE_HOME.'/nvim/tags'
  let g:gutentags_exclude_filetypes = [ 'gitcommit', 'text', 'markdown' ]
  let g:gutentags_modules = [ 'ctags' ]

Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'

call plug#end()

" ==============================================================================
" Lua setup
" ==============================================================================

lua << EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = {"bash", "c", "norg"},

  highlight = {
    enable = true
  },
  indent = {
    enable = true
  },
}
EOF

lua << EOF
require'gitsigns'.setup {
  signs = {
    add          = {hl = 'GitSignsAdd'   , text = '+', numhl='GitSignsAddNr'   , linehl='GitSignsAddLn'},
    change       = {hl = 'GitSignsChange', text = '~', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
    delete       = {hl = 'GitSignsDelete', text = '_', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    topdelete    = {hl = 'GitSignsDelete', text = '‾', numhl='GitSignsDeleteNr', linehl='GitSignsDeleteLn'},
    changedelete = {hl = 'GitSignsChange', text = '*', numhl='GitSignsChangeNr', linehl='GitSignsChangeLn'},
  },
  on_attach = function(bufnr)
    local gs = package.loaded.gitsigns

    local function map(mode, l, r, opts)
      opts = opts or {}
      opts.buffer = bufnr
      vim.keymap.set(mode, l, r, opts)
    end

    -- Navigation
    map('n', ']c', function()
      if vim.wo.diff then return ']c' end
      vim.schedule(function() gs.next_hunk() end)
      return '<Ignore>'
    end, {expr=true})

    map('n', '[c', function()
      if vim.wo.diff then return '[c' end
      vim.schedule(function() gs.prev_hunk() end)
      return '<Ignore>'
    end, {expr=true})

    -- Actions
    map({'n', 'v'}, '<leader>hs', ':Gitsigns stage_hunk<CR>')
    map({'n', 'v'}, '<leader>hr', ':Gitsigns reset_hunk<CR>')
    map('n', '<leader>hS', gs.stage_buffer)
    map('n', '<leader>hu', gs.undo_stage_hunk)
    map('n', '<leader>hR', gs.reset_buffer)
    map('n', '<leader>hp', gs.preview_hunk)
    map('n', '<leader>hb', function() gs.blame_line{full=false} end)
    map('n', '<leader>tb', gs.toggle_current_line_blame)
    map('n', '<leader>hd', gs.diffthis)
    map('n', '<leader>hD', function() gs.diffthis('~') end)
    map('n', '<leader>td', gs.toggle_deleted)

    -- Text object - not sure what this does...
    --map({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')
  end
}
EOF

lua << EOF
require'neorg'.setup {
  load = {
    ["core.defaults"] = {},
    ["core.dirman"] = {
      config = {
        workspaces = {
            work = "~/notes/work",
        }
      }
    },
  }
}
EOF

lua << EOF
require'gruvbox'.setup {
  undercurl = true,
  underline = true,
  bold = true,
  italic = {
    strings = true,
    operators = true,
    comments = true,
  },
  strikethrough = true,
  invert_selection = false,
  invert_signs = false,
  invert_tabline = false,
  invert_intend_guides = false,
  inverse = true, -- invert background for search, diffs, statuslines and errors
  contrast = "hard", -- can be "hard", "soft" or empty string
  palette_overrides = {},
  overrides = {
    Operator = {italic = false},
  },
  dim_inactive = false,
  transparent_mode = false,
}
EOF

" ==============================================================================
" === Settings
" ==============================================================================

" Files/Buffers
set autoread        " reload files changed outside of vim
set hidden          " hide buffers instead of closing them
set nobackup        " don't create backup files
set noswapfile      " don't create swap files
set undofile        " keep persistent undo history

" Cursor/Line
set number          " show line numbers
set relativenumber  " show relative line numbers
set cursorline      " highlight the current line
set showmatch       " highlight matching brackets
set scrolloff=999   " keep cursor centered vertically
set nowrap          " don't wrap lines

" Tabs/Indentation
set expandtab       " use spaces as tabs by default
set tabstop=8       " tabs get the default length of 8 spaces
set softtabstop=-1  " negative just uses the value of shiftwidth
set shiftwidth=4    " number of spaces for each step of indentation
set shiftround      " round indents to multiples of shiftwidth
set copyindent      " copy indentation for new lines
set smarttab        " <Tab> inserts blanks according to shiftwidth

" Search
set ignorecase      " ignore case when searching
set smartcase       " ignore case only when all lowercase
set hlsearch        " highlight search terms
set incsearch       " show search matches as you type

" Windows
set splitbelow      " new splits appear below current window
set splitright      " new vertical splits appear to the right
set updatetime=100  " milliseconds for the cursorhold event

" Statusline
set noshowmode      " don't print current mode
set laststatus=2    " always display statusline
set showtabline=2   " always display tabline

" Hidden characters
set list
set listchars=tab:•·,trail:·,extends:❯,precedes:❮,nbsp:×

" Misc
set timeoutlen=3000 " timeout for mappings
set lazyredraw      " only redraw the UI when needed
set completeopt=menu,noselect

" Enable true-color support if available
if has('termguicolors')
  set termguicolors
endif

" Colorscheme
set background=dark
syntax enable
colorscheme gruvbox

" Cursor style
set guicursor=n-v:block,i-c-ci-ve:ver25,r-cr:hor20,o:hor50
              \,a:blinkwait700-blinkoff400-blinkon250-Cursor/lCursor
              \,sm:block-blinkwait175-blinkoff150-blinkon175

" Set Search tool if available
if executable('rg') " ripgrep
  set grepprg=rg\ --vimgrep\ --no-heading
  set grepformat=%f:%l:%c:%m
elseif executable('pt') " the platinum searcher
  set grepprg=pt\ --nogroup\ --nocolor\ --ignore-case
elseif executable('ag') " the silver searcher
  set grepprg=ag\ --nogroup\ --nocolor
endif

" ===== Variables =====

" Vimtex autocompletion

" ==============================================================================
" === Autocommands
" ==============================================================================

if has('autocmd')

  " Terminal specific autocommands
  augroup term
    autocmd! BufWinEnter,WinEnter term://* startinsert
  augroup END

  " Sane wrapping for git commits
  augroup wrapping
    autocmd!
    autocmd FileType git,gitcommit setlocal wrap
    autocmd FileType git,gitcommit setlocal linebreak
    autocmd FileType git,gitcommit setlocal noexpandtab
    autocmd FileType git,gitcommit setlocal shiftwidth=8
  augroup END

  " Enable spelling and line breaks for text files
  augroup prose
    autocmd!
    autocmd FileType gitcommit,text,tex,plaintex,markdown setlocal spell
    autocmd FileType tex,plaintex,markdown setlocal textwidth=80
  augroup END

  " Easy quit windows
  augroup quit_windows
    autocmd!
    autocmd FileType qf,help,diff nnoremap <buffer><silent> q :q<CR>
  augroup END

  " Filetype overrides
  augroup filetypes
    autocmd!
    autocmd BufRead,BufNewFile *.h setlocal filetype=c
  augroup END

endif

" ==============================================================================
" === Keybindings
" ==============================================================================

" Make Y consistent with C and D
nnoremap Y y$

" Shortcut to edit init.vim
nnoremap <leader>v :tabedit $MYVIMRC<CR>

" Easier resize windows
nnoremap <silent> <leader>+ :resize +5<CR>
nnoremap <silent> <leader>- :resize -5<CR>
nnoremap <silent> <leader>> :vertical resize +5<CR>
nnoremap <silent> <leader>< :vertical resize -5<CR>

" Quick escape shortcuts for terminal mode
tnoremap <C-[> <C-\><C-n>
tnoremap <ESC> <C-\><C-n>

" Window navigations shortcuts
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

tnoremap <C-h> <C-\><C-n><C-w>h
tnoremap <C-j> <C-\><C-n><C-w>j
tnoremap <C-k> <C-\><C-n><C-w>k
tnoremap <C-l> <C-\><C-n><C-w>l

" Completion menu bindings
inoremap <expr><tab> pumvisible() ? "\<C-n>" : "\<tab>"
inoremap <expr><s-tab> pumvisible() ? "\<C-p>" : "\<s-tab>"

" QuickFix menu bindings
nnoremap <silent> <leader>q :copen<CR>
nnoremap <silent> <leader>l :lopen<CR>

" Clear search
nnoremap <silent> <leader>/ :let @/=''<CR>

" Ultisnips bindings
nnoremap <silent> <leader>se :UltiSnipsEdit<CR>

" Tagbar bindings
nnoremap <silent> <leader>t :TagbarToggle<CR>

" NERDTree bindings
nnoremap <silent> <leader>f :NERDTreeToggle<CR>
nnoremap <silent> <leader>F :NERDTreeFind<CR>

" UndoTree bindings
nnoremap <silent> <leader>u :UndotreeToggle<CR>

" FZF bindings
nnoremap <silent> <leader><space> :Files<CR>
nnoremap <silent> <leader>oo :Files $HOME<CR>
nnoremap <silent> <leader>ob :Buffers<CR>
nnoremap <silent> <leader>ow :Windows<CR>
nnoremap <silent> <leader>ot :Tags<CR>
nnoremap <silent> <leader>og :GFiles<CR>
nnoremap <silent> <leader>oc :Commits<CR>
nnoremap <silent> <leader>os :Snippets<CR>

" Git bindings
nnoremap <silent> <leader>gs :belowright Git<CR>
nnoremap <silent> <leader>gd :Gdiffsplit<CR>
nnoremap <silent> <leader>gc :Git commit<CR>
nnoremap <silent> <leader>gb :Git blame<CR>
nnoremap <silent> <leader>ge :Gedit<CR>
nnoremap <silent> <leader>gE :Gedit<space>
nnoremap <silent> <leader>gr :Gread<CR>
nnoremap <silent> <leader>gR :Gread<space>
nnoremap <silent> <leader>gw :Gwrite<CR>
nnoremap <silent> <leader>gW :Gwrite!<CR>
nnoremap <silent> <leader>gq :Gwq<CR>
nnoremap <silent> <leader>gQ :Gwq!<CR>
"nnoremap <silent> <leader>gt :GitGutterAll<CR>
